function getLastDayOfWeek(date) {
    console.log("getlast() : "+ date);
    
    var dt = moment().set({'year': Number(date.toString().substring(0,4)), 
                           'month' : Number(date.toString().substring(4,6)) -1 ,
                           'date' :Number(date.toString().substring(6,8))});
              
    dt = dt.endOf('week');
    dt = dt.add(1,'days').format("YYYYMMDD");
    
    return dt;
}

function getToday() {
    var today = new Date();
    
    today =  today.toISOString().slice(0,10).replace(/-/g,"");
    
    return today;
}

function getTargetDate(selectedDate) {
   var targetDate;
       
   if(moment().format("YYYYMMDD") == selectedDate) {
        targetDate =  moment().subtract(7, 'days').format("YYYYMMDD");
        
        showDate(targetDate);
    } else {
        targetDate = selectedDate;
    } 
    
    return targetDate;
}